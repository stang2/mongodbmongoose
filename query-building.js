const mongoose = require('mongoose')
const Building = require('./model/Building')
const Room = require('./model/Room')
mongoose.connect('mongodb://localhost:27017/Example')

async function main () {
  // Updated
  // const room = await Room.findById('6223aa0046de35c2ed19911b')
  // room.capacity = 300
  // room.save()
  // console.log(room)
  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('--------------------')
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const building = await Building.find({}).populate('rooms')
  console.log(JSON.stringify(building))
}

main().then(() => {
  console.log('Finish')
})
