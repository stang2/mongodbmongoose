const mongoose = require('mongoose')
const Building = require('./model/Building')
const Room = require('./model/Room')
mongoose.connect('mongodb://localhost:27017/Example')
async function main () {
  const newInformaticsBuilding = await Building.findById('6223a9ff46de35c2ed199119')
  const room = await Room.findById('6223aa0046de35c2ed19911b')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newInformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newInformaticsBuilding
  newInformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newInformaticsBuilding.save()
  informaticsBuilding.save()
}

main().then(() => {
  console.log('Finish')
})
